# coding=utf-8

import threading

try:
    import regex as re
    vt = tuple(int(i.strip()) for i in re.__version__.split('.'))
    if vt < (2, 4, 50):
        print('regex版本较低:%s, 使用内置re' % re.__version__)
        raise Exception
except:
    import re

#========================================
#       regular expression wrapper
#========================================

class red:
    A = re.A
    ASCII = re.ASCII

    DEBUG = re.DEBUG

    I = re.I
    IGNORECASE = re.IGNORECASE

    L = re.L
    LOCALE = re.LOCALE

    M = re.M
    MULTILINE = re.MULTILINE

    S = re.S
    DOTALL = re.DOTALL

    X = re.X
    VERBOSE = re.VERBOSE
    
    # cache
    regexs = dict()

    # threading lock
    lock = threading.Lock()

    @staticmethod
    def d(re_str, flags=0):
        '''compiled pattern cache'''
        red.lock.acquire()
        compiled = red.regexs.setdefault(
                        (re_str, flags),
                        re.compile(re_str, flags)
                        )
        red.lock.release()

        return compiled

    @staticmethod
    def sub(pattern, repl, string, count=0, flags=0):
        prog = red.d(pattern, flags)
        return prog.sub(repl, string, count=0)

    @staticmethod
    def clear_cache():
        red.lock.acquire()
        red.regexs.clear()
        red.lock.release()

