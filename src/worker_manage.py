# coding=utf-8
import sys
import threading
import time
import heapq

import bvars
from datadefine import *


class c_worker_exception(Exception):

    def __init__(self, title, url='', summary=''):
        self.title = title
        self.url = url
        self.summary = summary

    def __str__(self):
        s = '异常:' + self.title+'\n'
        if self.url:
            s += self.url+'\n'
        if self.summary:
            s += self.summary+'\n'
        return s


# 启动worker线程
def worker_starter(runcfg, source_id):

    def worker_wrapper(runcfg, 
                       worker, source, worker_dict,  
                       back_web_queue, bb_queue):

        #print('线程开始：%s' % source.source_id)

        int_time = round(time.time())
        lst = None
        except_mark = False

        try:
            lst = worker(source.data, worker_dict)

        except Exception as e:
            s = '源%s出现异常: ' % source.source_id
            print(s + str(e))

            except_mark = True

            i = c_info()
            i.title = '异常:' + e.title
            i.url = e.url or source.get('url', '')
            i.summary = e.summary
            i.suid = '<exception>'

            lst = [i]

        else:
            # callback函数
            if source.callback != None:
                local_d = locals()
                for i in lst:
                    local_d['info'] = i
                    exec(source.callback, globals(), local_d)
                    i = local_d['info']

                # remove info
                lst = [one for one in lst if one.temp != 'del']

        finally:
            # 通知执行结束
            c_message.make(bb_queue,
                           'bb:source_return',
                           source.source_id
                           )
            
            if not lst:
                print(source.source_id, '得到的列表为空')
                return

            # max length of info list
            if len(lst) > runcfg.max_entries:
                lst = lst[:runcfg.max_entries]

            # 处理内容
            for i in lst:
                i.source_id = source.source_id
                if not i.author:
                    i.author = source.name
                i.fetch_date = int_time

                if not i.suid:
                    print(i.source_id, '出现suid为空')

                # for html show
                i.summary = i.summary.replace('\n', '')
                i.summary = i.summary.replace('\r', '')

                i.pub_date = i.pub_date.replace('\n', '')
                i.pub_date = i.pub_date.replace('\r', '')

                # empty title
                if not i.title:
                    i.title = '<title>'

                # length
                if len(i.title) > runcfg.title_len:
                    i.title = i.title[:runcfg.title_len-3] + '...'
                    
                if len(i.summary) > runcfg.summary_len:
                    i.summary = i.summary[:runcfg.summary_len-3] + '...'

                if len(i.author) > runcfg.author_len:
                    i.author = i.author[:runcfg.author_len-3] + '...' 

                if len(i.pub_date) > runcfg.pub_date_len:
                    i.pub_date = i.pub_date[:runcfg.pub_date_len-3] + '...'    

            if not except_mark:
                c_message.make(back_web_queue,
                               'bw:del_exceptions_by_sid',
                               [source.source_id])
            
            c_message.make(back_web_queue, 
                           'bw:send_infos', 
                           lst)  

        #print('线程结束：%s' % source.source_id)        

    source = bvars.sources[source_id]

    worker_tuple = bvars.workers[source.worker_id]
    worker = worker_tuple[0]
    worker_dict = worker_tuple[1]

    t = threading.Thread(target=worker_wrapper, 
                         args=(runcfg,
                               worker, 
                               source, worker_dict,
                               bvars.back_web_queue, bvars.bb_queue), 
                         daemon=True
                         )
    t.start()


# for test source
def test_source(source_id):
    source = bvars.sources[source_id]

    worker_tuple = bvars.workers[source.worker_id]
    worker = worker_tuple[0]
    worker_dict = worker_tuple[1]

    int_time = round(time.time())


    # run
    try:
        lst = worker(source.data, worker_dict)

    except Exception as e:
        s = '源%s出现异常:\n' % source.source_id
        print(s + str(e))

    else:
        # callback函数
        if source.callback != None: 
            local_d = locals()
            for i in lst:
                local_d['info'] = i
                exec(source.callback, globals(), local_d)
                i = local_d['info']

            # remove info
            lst = [one for one in lst if one.temp != 'del']

        for i in lst:
            i.source_id = source.source_id
            if not i.author:
                i.author = source.name
            i.fetch_date = int_time

            if not i.suid:
                print(i.source_id, '出现suid为空')

            if len(i.title) > 70:
                i.title = i.title[:67] + '...'
                
            if len(i.summary) > 160:
                i.summary = i.summary[:157] + '...'

            if len(i.author) > 50:
                i.author = i.author[:47] + '...' 

            if len(i.pub_date) > 50:
                i.pub_date = i.pub_date[:47] + '...'    

        print('source_id:', source.source_id, 
              '\nlen:', len(lst), '\n')

        if len(lst) > 16:
            print_str = ''.join(str(i) for i in lst[:8]) + \
                        '...中间省略%d条...\n\n' % (len(lst)-16) + \
                        ''.join(str(i) for i in lst[-8:])
        else:
            print_str = ''.join(str(i) for i in lst)

     
        try:
            print(print_str)
        except UnicodeEncodeError:
            temp = str(print_str)
            for t in temp:
                try:
                    print(t, end='')
                except:
                    print('?', end='')
            print()


# for source-loading
def parse_data(worker_id, xml_string):
    parser = bvars.dataparsers[worker_id]

    d = parser(xml_string)
    return d

# worker function:
# params: (data_dict, worker_dict) 
# return: list(info) or c_worker_exception

# worker decorator
def worker(worker_id):
    def worker_decorator(func):
        if worker_id not in bvars.workers:
            bvars.workers[worker_id] = (func, dict())
        else:
            print('worker_id: %s already exist in workers' % worker_id)
    
    return worker_decorator

# dataparser function:
# params: (xml_string)
# return: data_dict

# data-parser decorator
def dataparser(worker_id):
    def dataparser_decorator(func):
        if worker_id not in bvars.dataparsers:
            bvars.dataparsers[worker_id] = func
        else:
            print('worker_id: %s already exist in dataparsers' % worker_id)
    
    return dataparser_decorator
