# coding=utf-8
import os
import codecs

from red import *

class c_user_cfg:
    __slots__ = ('username', 'password', 
                 'col_per_page', 'usertype',
                 'category_list')

    def __init__(self):
        self.username = ''
        self.password = ''
        self.col_per_page = 18

        # 0:public, 1:normal, 2:admin
        self.usertype = 1

        # 组织结构列表
        # 列表的元素为tuple: (category, <list>)
        # <list>的元素为list: 
        # [sid, level, interval, name, comment, link]
        self.category_list = list()

    @staticmethod
    def load_users():
        root_path = os.path.dirname(os.path.abspath(__file__))
        root_path = os.path.dirname(root_path)
        users_path = os.path.join(root_path, 'cfg')

        user_cfg_list = list()

        for item in os.listdir(users_path):
            fpath = os.path.join(users_path, item)
            if os.path.isfile(fpath) and \
               item.lower().endswith('.txt'):
                    user_cfg = c_user_cfg.parse_cfg(fpath, item)
                    if user_cfg:
                        user_cfg_list.append(user_cfg)

        return user_cfg_list
                 
    @staticmethod
    def parse_cfg(f_fullpath, f_filename):

        def is_category(line):
            p = red.d(r"^'.*'$")
            if p.match(line):
                return True
            else:
                return False

        # load file
        try:
            f = open(f_fullpath, 'rb')
            byte_data = f.read()
            f.close()
        except Exception as e:
            print('读取文件%s时出错' % f_filename, str(e))
            return None

        # decode
        try:
            if len(byte_data) >= 3 and byte_data[:3] == codecs.BOM_UTF8:
                byte_data = byte_data[3:]

            text = byte_data.decode('utf-8')
        except Exception as e:
            print('文件%s解码失败，请确保是utf-8编码' % f_filename, str(e))
            return None
       
        # to \n 
        text = text.replace('\r\n', '\n')
        text = text.replace('\r', '\n')

        lines = text.split('\n')

        user = c_user_cfg()
        # username
        user.username = f_filename.lower().rstrip('.txt')

        orgnise_started = False
        current_category = None
        for i, line in enumerate(lines):
            line = line.strip()
            if not line or line.startswith('#'):
                continue

            if not orgnise_started:
                if line.startswith('[organise]'):
                    orgnise_started = True
                    continue

                split_lst = line.split('=', 1)
                k = split_lst[0].strip()
                v = split_lst[1].strip()

                if k == 'password' and len(v) >= 2 and \
                   v.startswith("'") and v.endswith("'"):
                    user.password = v[1:-1]
                elif k == 'col_per_page':
                    try:
                        user.col_per_page = int(v)
                    except:
                        pass
                elif k == 'usertype':
                    if v == 'public':
                        user.usertype = 0
                    elif v == 'normal':
                        user.usertype = 1
                    elif v == 'admin':
                        user.usertype = 2
                else:
                    s = '文件%s出现错误，未知键值:\n%s'
                    print(s % (f_filename, k))

            # orgnise started
            else:
                if is_category(line):
                    user.category_list.append( (line[1:-1], list()) )
                    current_category = user.category_list[-1][1]

                else:
                    r = r"^\s*'(.*?)'\s*,\s*(\d+)\s*,\s*(\d+)\s*(?:#.*)?$"
                    p = red.d(r)
                    m = p.search(line)

                    if m:
                        sid = m.group(1).lower()
                        level = int(m.group(2))
                        interval = int(m.group(3))

                        if current_category != None:
                            current_category.append([sid, level, interval,
                                                    'name', 'comment', 'link'])
                        else:
                            s = '文件%s出现错误，缺少分类:\n%s'
                            print(s % (f_filename, line))

                    else:
                        s = '文件%s出现错误，分类格式有误'
                        print(s % f_filename)

        return user
