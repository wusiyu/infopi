# coding=utf-8

import sys

import workers

import source_manage
source_manage.load_sources()

from worker_manage import test_source

def main():
    if len(sys.argv) > 1:
        arg1 = sys.argv[1]
        test_source(arg1)

if __name__ == '__main__':
    main()